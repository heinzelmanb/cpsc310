'''
    Authors: Bert Heinzelman, Bradley Carrion
    Date: October 4, 2016
    Assignment: hw3
    File: hw3.py

    mpg, cylinders, displacement, horsepower, weight, acceleration, model year, origin, car name

'''

import numpy
import csv
from tabulate import tabulate
from random import randint
import copy

'''
    class to handle table operations
'''


class Table(object):

    def __init__(self, **kwargs):
        file = kwargs.get('file', None)
        inTable = kwargs.get('table', None)

        if file is not None:
            self.table = self.get_table(file)
        elif inTable is not None:
            self.table = inTable

        self.file = file

    def get_table(self, file):
        with open(file, 'r') as f:
            reader = csv.reader(f, dialect="excel")

            return [row for row in reader if len(row) > 0]
    '''
        returns a lists of lists grouped together based on
        another attribute
    '''

    def group_by(self, index):
        groups = {}
        for row in self.table:
            if row[index] in groups:
                groups[row[index]].append(row)
            else:
                groups[row[index]] = [row]
        # fix maybe
        group_list = sorted(
            [groups[key] for key in groups.keys()], key=lambda row: row[0][index])
        #group_list = [groups[key] for key in groups.keys()]
        return [Table(table=group) for group in group_list]

    '''
        returns the number of instances in a table, int
    '''

    def count_rows(self):
        return len(self.table)

    def get_column(self, column):
        return [float(row[column])
                for row in self.table if row[column] != 'NA']

    # return the min of the table
    def min(self, column):
        return min(self.get_column(column))

    # returns the max of the table
    def max(self, column):
        return max(self.get_column(column))

    # returns the midpoint of the table
    def average(self, column):
        col = self.get_column(column)
        entries = len(col)
        return sum(col) / float(entries)

'''
This method converts continuous data to categorical data
then returns the new list of categorical values. And it does
this conversion by using the map function parameter passed in.
'''


def continuous_to_categorical(table, index, map_func):
    table_copy = copy.deepcopy(table.table)
    for row in table_copy:
        row[index] = map_func(float(row[index]))

    return table_copy

'''
This method maps the continuous values to
specific categorical values based on the value.
'''


def map_mpg(value):
    if value >= 45:
        return 10
    elif value >= 37:
        return 9
    elif value >= 31:
        return 8
    elif value >= 27:
        return 7
    elif value >= 24:
        return 6
    elif value >= 20:
        return 5
    elif value >= 17:
        return 4
    elif value >= 15:
        return 3
    elif value >= 14:
        return 2
    else:
        return 1


'''
This method uses the least squares method to help
predict new record values.
'''


def least_squares(table, m_index, base_index, instance):
    xs = table.get_column(base_index)
    ys = table.get_column(m_index)
    x_avg = table.average(base_index)
    y_avg = table.average(m_index)

    numerator = float(sum([(xs[i] - x_avg) * (ys[i] - y_avg)
                           for i in range(len(xs))]))
    denomenator = float(sum([(xi - x_avg)**2 for xi in xs]))

    # calculate the slope
    m = numerator / denomenator

    # calculate the y-intercept
    b = y_avg - m * x_avg

    return m * float(instance[base_index]) + b


def knn_classifer(table, m_index, instance, idxs, k):
    closest = knn(table, instance, k, idxs)

    # compute the weighted average
    avg = sum([(table.table[closest[i][0]][m_index]) * (k - i)
               for i in range(0, k)]) / (sum(range(1, k + 1)))

    return avg


'''
    Normalize a value with respec to its min and max
'''


def normalize(value, min_val, max_val):
    return (float(value) - float(min_val)) / (float(max_val) - float(min_val))

'''
    Given a table, and a row:
    Returns the k closest rows on the given attributes
'''


def knn(table, predicterRow, k, attributes):
    attribute_count = len(table.table[0])

    min_max = {i: (table.min(i), table.max(i)) for i in attributes}

    distances = []

    for j in range(0, len(table.table)):
        row = table.table[j]
        distance = 0
        for i in attributes:
            training_val = row[i]
            p_val = predicterRow[i]

            normalized_t = normalize(training_val, min_max[
                                     i][0], min_max[i][1])
            normalized_p = normalize(p_val, min_max[i][0], min_max[i][1])

            distance += (normalized_t - normalized_p)**2
        distances.append((j, distance**(.5)))
    #distances = sorted(distances, key=lambda x: x[1])

    nearest = []
    for i in range(0, k + 1):
        nearest.append(min(distances, key=lambda x: x[1]))

    # exclude first elem because it is equal to the row you are trying to
    # find the neighbors of.
    return nearest[1:]


'''
    given a list of results for the different classes,
    the accuracy is returned
'''


def multi_class_accuracy(results):
    def accuracy(row):
        top = row[2] + row[3]
        bottom = row[0] + row[1]
        return top / float(bottom)

    avg = 0
    for i in range(1, 11):
        avg += accuracy(results[i])
    return avg / 10.0

'''
    given a list of results for the different classes,
    the error rate is returned
'''


def error_rate(results):
    return 1 - multi_class_accuracy(results)

'''
    Will compute the results given an actual value
    and the guess.
'''


def add_class_result(results, actual, guess):
    # (P, N, TP, TN)
    res = results[actual]

    if actual == guess:
        results[actual] = (res[0] + 1, res[1], res[2] + 1, res[3])

        # update for other classes
        for i in range(1, 11):
            if i != actual:
                res2 = results[i]
                results[i] = (res2[0], res2[1] + 1, res2[2], res2[3] + 1)

    else:
        results[actual] = (res[0], res[1] + 1, res[2], res[3])

    return results


'''
    Returns a Random subsampling
'''


def random_subsample(table, fraction):
    copy_table = copy.deepcopy(table)

    test_set = []

    # adds rows to test set, deletes from table
    while len(test_set) < len(table.table) * fraction:
        idx = randint(0, len(copy_table.table) - 1)
        test_set.append(copy_table.table[idx])

        del copy_table.table[idx]

    return (test_set, copy_table)


'''
    Given the partition list, i.e. [d1, d2, ..., dk]
    this function will return a tuple containing di,
    and the union of the other tables, which will be
    the training set.
'''


def cross_validation(partitions, i):
    cpy = copy.deepcopy(partitions)

    test = partitions[i]
    training = []

    # build up union of partitions for training set
    for j in range(0, len(cpy)):
        if i != j:
            training += cpy[j]

    return (test, Table(table=training))


'''
    Partitions the dataset into k pieces,
    Keeps the distribution somewhat the same
    as the initial dataset
'''


def cross_validation_partitions(table, k, index):
    freq = mpg_freq_per_group(table, k, index)

    groups = copy.deepcopy(table.group_by(index))

    partitions = []

    # builds each partition with the same ratio of each class
    for i in range(0, k):
        partitions.append([])
        # grabs the correct distribution
        for key in freq.keys():
            values_to_get = freq[key]
            # puts the correct distribution in each partition
            for j in range(0, values_to_get):
                partitions[-1].append(groups[key - 1].table[0])
                del groups[key - 1].table[0]

    # sprinkle the rest in some partition
    flat = [val for g in groups for val in g.table]  # flatten list

    i = 0
    for val in flat:
        partitions[i % 10].append(val)
        i += 1

    return partitions


'''
    returns a list where the value for a
    particular index is the amount of values
    a partition should contain for the class
    value that equals that index
'''


def mpg_freq_per_group(table, k, index):
    freq = {}

    space = table.count_rows() / k

    # count different classes
    for row in table.table:
        if int(row[index]) in freq:
            freq[int(row[index])] += 1
        else:
            freq[int(row[index])] = 1

    # change to percentages
    for key in freq.keys():
        freq[key] /= float(table.count_rows())
        freq[key] *= space
        freq[key] = int(freq[key])

    return freq


def classify(m_index, base_index, predictor, instance, table):
    predicted_val = predictor(table, m_index, base_index, instance)

    # insert the new val into the instance
    # instance[m_index] = predicted_val

    return predicted_val


def step1(table):
    print "========================================"
    print "STEP 1: Linear Regression MPG Classifier"
    print "========================================"

    p_index = 0  # predicted index, MPG
    b_index = 4  # base index, weight
    num_tests = 5  # number of instances to test

    # categorize the data assuming its continuous
    categorical_table = Table(
        table=continuous_to_categorical(table, p_index, map_mpg))

    for i in range(0, num_tests):
        rand_index = randint(0, len(categorical_table.table) - 1)
        instance = categorical_table.table[rand_index]
        s_instance = (str(instance)[1:-1]).replace('\'', '')
        print 'instance: ' + s_instance
        predicted_val = classify(
            p_index, b_index, least_squares, instance, categorical_table)
        print 'class: ' + str(int(round(predicted_val))) + ', actual: ' + str(instance[p_index])


def step2(table):
    print "\n==========================================="
    print "STEP 2: k=5 Nearest Neighbor MPG Classifier"
    print "==========================================="

    p_index = 0  # predicted index, MPG
    num_tests = 5  # number of instances to test
    k = 5

    # categorize the data assuming its continuous
    categorical_table = Table(
        table=continuous_to_categorical(table, p_index, map_mpg))

    for i in range(0, num_tests):
        rand_index = randint(0, len(categorical_table.table) - 1)
        instance = categorical_table.table[rand_index]
        predicted_val = knn_classifer(
            categorical_table, p_index, instance, [1, 4, 5], k)
        s_instance = (str(instance)[1:-1]).replace('\'', '')
        print 'instance: ' + s_instance
        print 'class: ' + str(int(predicted_val)) + ', actual: ' + str(instance[p_index])


'''
    given a training set and a test set,

    will compute the accurarcy's and error rates
    for both knn and linear regression

    returns (knn_accuracy, knn_error, regression_accuracy, regression_error)
'''


def predictive_accuracy(test, training, con_reg=None, con_knn=None):
    # (P, N, TP, TN), data structure to store results for each class
    knn_avg, reg_avg = 0, 0
    knn_err, reg_err = 0, 0

    k = 10
    p_index = 0  # predicted index, MPG
    b_index = 4  # base index, weight

    knn_results = {i: (0, 0, 0, 0) for i in range(1, 11)}
    reg_results = {i: (0, 0, 0, 0) for i in range(1, 11)}

    # calculates statistics for each row the test set
    for row in test:
        predicted_val_knn = knn_classifer(training, p_index, row, [1, 4, 5], 5)
        predicted_val_reg = classify(
            p_index, b_index, least_squares, row, training)
        predicted_val_reg = int(round(predicted_val_reg))

        knn_results = add_class_result(knn_results, row[0], predicted_val_knn)
        reg_results = add_class_result(reg_results, row[0], predicted_val_reg)

        # keeps track of the data for the confusion matrices
        if con_reg is not None and con_knn is not None:
            con_reg[row[0] - 1][predicted_val_reg] += 1
            con_knn[row[0] - 1][predicted_val_knn] += 1

    knn_avg += multi_class_accuracy(knn_results)
    knn_err += error_rate(knn_results)

    reg_avg += multi_class_accuracy(reg_results)
    reg_err += error_rate(reg_results)

    return (knn_avg, knn_err, reg_avg, reg_err)


def step3a(table):
    print "\tRandom Subsample (k=10, 2:1 Train/Test)"

    knn_avg, reg_avg = 0, 0
    knn_err, reg_err = 0, 0

    k = 10
    p_index = 0  # predicted index, MPG
    # b_index = 4 # base index, weight

    categorical_table = Table(
        table=continuous_to_categorical(table, p_index, map_mpg))

    # loops through test set and randomly subsamples
    for j in range(0, k):
        test, training = random_subsample(
            categorical_table, 1.0 / 3.0)  # test set, training table

        res = predictive_accuracy(test, training)

        knn_avg += res[0]
        knn_err += res[1]

        reg_avg += res[2]
        reg_err += res[3]

    reg_avg /= float(k)
    reg_err /= float(k)

    knn_avg /= float(k)
    knn_err /= float(k)

    print "\t\tLinear Regression: accuracy = ",
    print reg_avg,
    print ", error rate = ",
    print reg_err

    print "\t\tK Nearest Neighbors: accuracy = ",
    print knn_avg,
    print ", error rate = ",
    print knn_err


def step3b(table):
    print "\tStratified 10-Fold Cross Validation:"

    # how many groups we want to split into
    k = 10

    p_index = 0  # predicted index, MPG

    knn_avg, reg_avg = 0, 0
    knn_err, reg_err = 0, 0

    categorical_table = Table(
        table=continuous_to_categorical(table, p_index, map_mpg))

    partitions = cross_validation_partitions(categorical_table, k, 0)

    confusion_reg = [[0 for i in range(1, 14)] for j in range(0, 10)]
    confusion_knn = [[0 for i in range(1, 14)] for j in range(0, 10)]

    for i in range(0, k):
        test, training = cross_validation(partitions, i)

        res = predictive_accuracy(test, training, confusion_reg, confusion_knn)

        knn_avg += res[0]
        knn_err += res[1]

        reg_avg += res[2]
        reg_err += res[3]

    reg_avg /= float(k)
    reg_err /= float(k)

    knn_avg /= float(k)
    knn_err /= float(k)

    print "\t\tLinear Regression: accuracy = ",
    print reg_avg,
    print ", error rate = ",
    print reg_err

    print "\t\tK Nearest Neighbors: accuracy = ",
    print knn_avg,
    print ", error rate = ",
    print knn_err

    return (confusion_reg, confusion_knn)


def step3(table):
    print "\n========================================"
    print "STEP 3: Predictive Accuracy"
    print "========================================"
    step3a(table)
    return step3b(table)


def step4a(table):
    pass


def step4b(table):
    pass


def step4(confusion_matrices):
    print "\n========================================"
    print "STEP 4: Confusion Matrices"
    print "========================================"

    '''
    Linear Regression Confusion Matrix
    '''

    # formate table
    for i in range(0, 10):
        # initialize the first column
        confusion_matrices[0][i][0] = i + 1
        confusion_matrices[1][i][0] = i + 1

        # compute total column
        confusion_matrices[0][i][11] = sum(confusion_matrices[0][i][1:10])
        confusion_matrices[1][i][11] = sum(confusion_matrices[1][i][1:10])

        # compute recognition (%) column
        # placed in try/catch blocks in special case of divide by 0 --> lazy
        # but works
        try:
            confusion_matrices[0][i][12] = (float(confusion_matrices[0][i][
                                            i + 1]) / float(confusion_matrices[0][i][11])) * 100
        except:
            confusion_matrices[0][i][12] = 'NA'
        try:
            confusion_matrices[1][i][12] = (float(confusion_matrices[1][i][
                                            i + 1]) / float(confusion_matrices[1][i][11])) * 100
        except:
            confusion_matrices[1][i][12] = 'NA'

    print "Linear Regression (Stratified 10-Fold Cross Validation Results):"
    headers = ["MPG"]
    headers.extend(list(range(1, 11)))
    headers.extend(["Total", "Recognition (%)"])
    print tabulate(confusion_matrices[0], headers, tablefmt="rst")

    '''
    KNN Confusion Matrix
    '''

    print "\nK-NN (Stratified 10-Fold Cross Validation Results):"
    print tabulate(confusion_matrices[1], headers, tablefmt="rst")


if __name__ == '__main__':
    table = Table(file='auto-data.txt')
    step1(table)
    step2(table)
    c_matrices = step3(table)
    step4(c_matrices)
