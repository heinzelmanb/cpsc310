'''
    Authors: Bert Heinzelman, Bradley Carrion
    Date: 10/25/16
    Assignment: 5
'''

import numpy
from Table import Table
from Classifiers import DecisionTreeClassifier, RandomForestClassifier
from Test import CrossValidation, StratifiedSample
import copy

'''
    Given a weight, discritize it
'''
def weight_discritize(val):
    val = float(val)
    if val >= 3500:
        return 5
    elif val >= 3000:
        return 4
    elif val >= 2500:
        return 3
    elif val >= 2000:
        return 2
    return 1

'''
    Given an mpg, discritize it
'''
def mpg_discritize(value):
    value = float(value)
    if value >= 45:
        return 10
    elif value >= 37:
        return 9
    elif value >= 31:
        return 8
    elif value >= 27:
        return 7
    elif value >= 24:
        return 6
    elif value >= 20:
        return 5
    elif value >= 17:
        return 4
    elif value >= 15:
        return 3
    elif value >= 14:
        return 2
    else:
        return 1

def discritize(table, function, idx):
    table_copy = copy.deepcopy(table)

    for row in table_copy.table:
        row[idx] = function(row[idx])
    return table_copy

# calculates the avg accuracy of n forests
def getAvgAccuracy(forests):
    total = 0
    for i in range(0, len(forests)):
        total += forests[i].accuracy()
    return total/len(forests)

def run_classifier_test(N, M, F, dataset, attrs, idx, display=True, mxN=0, mxM=0, size=3):
    test, remainder = StratifiedSample.stratified_sample(dataset, size, idx)

    domain = dataset.get_domain(attrs + [idx])

    # test the normal decision tree against the test data
    tree_classifier = DecisionTreeClassifier(idx, attrs, remainder, domain=domain)
    dtree = CrossValidation(tree_classifier, 10, test)
    dtree.run_test(display=True)

    if (mxN == 0): # only create one random forest classifier
        rf_classifier = RandomForestClassifier(idx, attrs, remainder, N, M, F, domain=domain)
        rforest = CrossValidation(rf_classifier, 10, test)
        rforest.run_test(display=display)
    else:
        print "analyzing..."
        best_forest = []
        best_accuracy = 0
        temp_forest = []
        best_n, best_m, best_f = 0, 0, 0
        for newN in range(N, mxN, 10):
            for newM in range(M, mxM, 5):
                for newF in range(F, len(attrs)+1):
                    for i in range(0, 5):
                        rf_classifier = RandomForestClassifier(idx, attrs, remainder, N, M, F, domain=domain)
                        rforest = CrossValidation(rf_classifier, 10, test)
                        rforest.run_test(display=display, domain=domain)
                        temp_forest.append(rforest)
                        if len(temp_forest) == 5:
                            temp_accuracy = getAvgAccuracy(temp_forest)
                            # check if best_tree is assigned to anything
                            if best_accuracy < temp_accuracy:
                                best_accuracy = temp_accuracy
                                best_forest = temp_forest
                                best_n, best_m, best_f = newN, newM, newF
                                temp_forest = []


        print "N = " + str(best_n) + ", M = " + str(best_m) + ", F = " + str(best_f)
        print "These N, M, and F values have the best average accuracy overall of: " + str(best_accuracy)
        for i in range(0, 5):
            print best_forest[i].accuracy()


def step2():

    print '================================================='
    print 'TITANIC DATA STEP 2'
    print '================================================='

    titanic_data = Table(file="titanic.txt")
    titanic_data.table = titanic_data.table[1:]

    TITANIC_CLS_IDX = 3
    TITANIC_ATS = [0, 1, 2]

    run_classifier_test(20, 7, 2, titanic_data, TITANIC_ATS, TITANIC_CLS_IDX)


    print '================================================='
    print 'AUTO DATA STEP 2'
    print '================================================='

    AUTO_ATS = [1, 4, 6]
    AUTO_CLS_IDX = 0

    # set up auto data table
    auto_data = discritize(Table(file="auto-data.txt"), weight_discritize, 4)
    auto_data = discritize(auto_data, mpg_discritize, 0)

    run_classifier_test(20, 7, 2, auto_data, AUTO_ATS, AUTO_CLS_IDX)


def step4():
    print '================================================='
    print 'WISCONSIN DATA STEP 4'
    print '================================================='

    w_data = Table(file="wisconsin.csv")

    W_CLS_IDX = 9
    W_ATS = range(9)

    run_classifier_test(50, 20, 3, w_data, W_ATS, W_CLS_IDX)


def step3():
    print '================================================='
    print 'AUTO DATA STEP 3'
    print '================================================='

    AUTO_ATS = [1, 4, 6]
    AUTO_CLS_IDX = 0

    # set up auto data table
    auto_data = discritize(Table(file="auto-data.txt"), weight_discritize, 4)
    auto_data = discritize(auto_data, mpg_discritize, 0)

    run_classifier_test(25, 5, 3, auto_data, AUTO_ATS, AUTO_CLS_IDX, False, 30, 10)


    print '================================================='
    print 'TITANIC DATA STEP 3'
    print '================================================='

    titanic_data = Table(file="titanic.txt")
    titanic_data.table = titanic_data.table[1:]

    TITANIC_CLS_IDX = 3
    TITANIC_ATS = [0, 1, 2]

    run_classifier_test(25, 5, 3, titanic_data, TITANIC_ATS, TITANIC_CLS_IDX, False, 30, 10, 10)


if __name__ == "__main__":
    step2()
    step3()
    step4()
