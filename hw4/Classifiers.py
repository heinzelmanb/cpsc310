import numpy as np
import math

'''
    Author Bert Heinzelman, Brad Carrion
    Date: 10/14/16
    Description: file to hold all classifiers
'''


'''
    Base class for classifier object.
    given a table, idx, and set of attributes,
    this class will classify a given rows label
    at index idx
'''
class Classifier(object):
    def __init__(self, idx, attributes):
        self.idx = idx
        self.attributes = attributes

    '''
        params:
            row: the row to classify
            attributes: the rows to include in the classification
            idx: the attribute to predict
    '''
    def classify(self, row, idx):
        raise Exception("Method not implemented in base class")


'''
    Uses bayes method as a classification technique
'''
class BayesianClassifier(Classifier):

    def __init__(self, idx, attributes):
        super(BayesianClassifier, self).__init__(idx, attributes)

    def classify(self, row, table):

        probabilities = []

        for cls in table.get_vals(self.idx):
            probabilities.append((self.prob(row, cls, table), cls))

        probabilities.sort(key=lambda x: x[0])
        return probabilities[-1][1]

    '''
        P(CLASS, ROW)
    '''
    def prob(self, row, cls, table):
        return self.prob_x_h(row, cls, table) * self.prob_h(cls, table)

    '''
        P(ROW, CLASS) with independence assumption
    '''
    def prob_x_h(self, row, cls, table):
        groups = table.group_by(self.idx)

        prob = 1
        for i in self.attributes:
            # if i == self.idx:
            #     continue
            vi = row[i]

            rows_with_cls = groups[cls-1].table

            matches = len(filter(lambda ins: ins[i] == vi, rows_with_cls))

            prob *= (float(matches)/len(rows_with_cls))

        return prob

    '''
        P(CLASS)
    '''
    def prob_h(self, cls, table):
        groups = table.group_by(self.idx)

        rows_with_cls = groups[cls-1].table
        return len(rows_with_cls)/float(len(table.table))

    def __str__(self):
        return "Naive Bayes"

'''
    Uses Bayisian classification, but uses gaussian function
    for continuous attributes
'''
class DiverseBayesianClassifier(BayesianClassifier):
    def __init__(self, idx, cont_attrs, cat_attrs):
        super(DiverseBayesianClassifier, self).__init__(idx, cat_attrs)
        self.cont_attrs = cont_attrs

    def prob_x_h(self, row, cls, table):
        prob = super(DiverseBayesianClassifier, self).prob_x_h(row, cls, table)

        for i in self.cont_attrs:
            stdev = np.std(table.get_column(i))
            mean = table.average(i)

            prob *= self.gaussian(float(row[i]), mean, stdev)

        return prob

    def gaussian(self, value, mean, stdev):
        first, second = 0, 0

        if stdev > 0:
            first = 1/(math.sqrt(2*math.pi)*stdev)
            second = math.e**((-(value-mean)**2)/(2*(stdev**2)))

        return first*second


'''
    Classification using KNN
'''
class KNNClassifier(Classifier):
    def __init__(self, idx, attributes, k):
        super(KNNClassifier, self).__init__(idx, attributes)
        self.k = k

    def classify(self, row, table):
        closest = self.knn(table, row, self.k, self.attributes)

        # compute the weighted average
        avg = sum([(table.table[closest[i][0]][self.idx]) * (self.k - i)
                   for i in range(0, self.k)]) / (sum(range(1, self.k + 1)))

        return avg

    def knn(self, table, predicterRow, k, attributes):
        attribute_count = len(table.table[0])

        min_max = {i: (table.min(i), table.max(i)) for i in attributes}

        distances = []

        for j in range(0, len(table.table)):
            row = table.table[j]
            distance = 0
            for i in attributes:
                training_val = row[i]
                p_val = predicterRow[i]

                normalized_t = self.normalize(training_val, min_max[
                                         i][0], min_max[i][1])
                normalized_p = self.normalize(p_val, min_max[i][0], min_max[i][1])

                distance += (normalized_t - normalized_p)**2
            distances.append((j, distance**(.5)))
        #distances = sorted(distances, key=lambda x: x[1])

        nearest = []
        for i in range(0, k + 1):
            nearest.append(min(distances, key=lambda x: x[1]))

        # exclude first elem because it is equal to the row you are trying to
        # find the neighbors of.
        return nearest[1:]

    '''
        Normalize a value with respec to its min and max
    '''
    def normalize(self, value, min_val, max_val):
        return (float(value) - float(min_val)) / (float(max_val) - float(min_val))

    def __str__(self):
        return "K Nearest Neighbors"
