'''
    Authors: Bert Heinzelman, Bradley Carrion
    Date: 10/14/16
    Assignment: 4


    mpg (miles per gallon), cylinders, displacement, horsepower, weight,
    acceleration, model year, origin, and car name
'''

import numpy
from Table import Table
from tabulate import tabulate
from random import randint
import copy

from Classifiers import BayesianClassifier, DiverseBayesianClassifier, KNNClassifier
from Test import RandomTest, RandomSubsample, CrossValidation

'''
    Given a weight, discritize it
'''
def weight_discritize(val):
    val = float(val)
    if val >= 3500:
        return 5
    elif val >= 3000:
        return 4
    elif val >= 2500:
        return 3
    elif val >= 2000:
        return 2
    return 1

'''
    Given an mpg, discritize it
'''
def mpg_discritize(value):
    value = float(value)
    if value >= 45:
        return 10
    elif value >= 37:
        return 9
    elif value >= 31:
        return 8
    elif value >= 27:
        return 7
    elif value >= 24:
        return 6
    elif value >= 20:
        return 5
    elif value >= 17:
        return 4
    elif value >= 15:
        return 3
    elif value >= 14:
        return 2
    else:
        return 1

# titanic class discritization (1=crew, 2=first, 3=second, 4=third)
def titanic_discritize1(value):
    if value == "crew":
        return 1
    elif value == "first":
        return 2
    elif value == "second":
        return 3
    elif value == "third":
        return 4

# titanic age discritization (1=adult, 2=child)
def titanic_discritize2(value):
    if value == "adult":
        return 1
    elif value == "child":
        return 2

# titanic sex discritization (1=male, 2=female)
def titanic_discritize3(value):
    if value == "male":
        return 1
    elif value == "female":
        return 2

# titanic survived discritization (1=yes, 2=no)
def titanic_discritize4(value):
    if value == "yes":
        return 1
    elif value == "no":
        return 2

def discritize(table, function, idx):
    table_copy = copy.deepcopy(table)

    for row in table_copy.table:
        row[idx] = function(row[idx])
    return table_copy



'''
    Main function
'''
if __name__ == '__main__':
    # predict mpg(0) from cylinders(1), weight(4), model year(6),

    '''
    STEP 1
    '''
    auto_data = discritize(Table(file="auto-data.txt"), weight_discritize, 4)
    auto_data = discritize(auto_data, mpg_discritize, 0)

    classifier = BayesianClassifier(0, [1, 4, 6])

    step1a = RandomTest(classifier, auto_data)
    step1a.run_test()

    step1b = RandomSubsample(classifier, 5, 1.0/3.0, auto_data)
    step1b.run_test()

    step1c = CrossValidation(classifier, 10, auto_data)
    step1c.run_test()

    '''
    STEP 2
    '''

    auto_data2 = discritize(Table(file="auto-data.txt"), mpg_discritize, 0)

    mixed_classifier = DiverseBayesianClassifier(0, [4], [1, 6])

    step2a = RandomTest(mixed_classifier, auto_data2)
    step2a.run_test()

    step2b = RandomSubsample(mixed_classifier, 5, 1.0/3.0, auto_data2)
    step2b.run_test()

    step2c = CrossValidation(mixed_classifier, 10, auto_data2)
    step2c.run_test()

    '''
    STEP 3
    '''

    titanic_data = Table(file="titanic.txt")
    titanic_data.table = titanic_data.table[1:]
    titanic_data = discritize(titanic_data, titanic_discritize1, 0)
    titanic_data = discritize(titanic_data, titanic_discritize2, 1)
    titanic_data = discritize(titanic_data, titanic_discritize3, 2)
    titanic_data = discritize(titanic_data, titanic_discritize4, 3)

    baye_classifier = BayesianClassifier(3, [0, 1, 2])
    knn_classifer = KNNClassifier(3, [0, 1, 2], 10)

    step3a = RandomTest(baye_classifier, titanic_data)
    step3a.run_test()
    step3b = RandomSubsample(baye_classifier, 5, 1.0/3.0, titanic_data)
    step3b.run_test()
    step3c = CrossValidation(baye_classifier, 10, titanic_data)
    step3c.run_test()

    step3a = RandomTest(knn_classifer, titanic_data)
    step3a.run_test()
    step3b = RandomSubsample(knn_classifer, 5, 1.0/3.0, titanic_data)
    step3b.run_test()
    step3c = CrossValidation(knn_classifer, 10, titanic_data)
    step3c.run_test()
