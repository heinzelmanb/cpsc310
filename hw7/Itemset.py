from math import pow
from itertools import combinations as combs
from tabulate import tabulate

'''
Class to handle all operations on an itemset
'''
class Itemset(object):
    def __init__(self, **kwargs):
        # this table should be the Table object itself
        self.table = kwargs.get('table', None)
        self.attrs = kwargs.get('attrs', None)
        self.itemset = self.buildSet()
        self.N = len(self.itemset)
        self.domain = self.calculate_domain(self.itemset)

        self.sets = [set(item) for item in self.itemset]

        # cache up the counts to speed it up!
        self.count_cache = {}

    '''
    This builds up the set that represents the market-basket analysis
    the format is:
        set -- one for the entire itemset
        (
            tuples -- for ea. transaction
            (
                    tuples(idx, val) for ea. item in a transaction
            )
        )
    '''
    def buildSet(self):
        # attribute indexes used for tuples in set
        attrs = range(0,len(self.table.table[0]))
        itemset = list()
        for transaction in self.table.table:
            curr_transaction = tuple()
            for idx in attrs:
                # append (index value, content)
                curr_transaction = curr_transaction + ((idx, transaction[idx]),)
            itemset.append(curr_transaction)
        return itemset

    '''
    This function will calculate the domain of the entire dataset
    and by domain, we mean:
        "All possible items that can be 'bought' in the market-basket analysis"
    '''
    def calculate_domain(self, itemset):
        domain = set()
        for transaction in itemset:
            for item in transaction:
                domain.add(item)
        return domain

    '''
    support(S) =   count(S) / N

    s - the set of the Left U Right
    N - the number of transactions in the itemset
    '''
    def support(self, s):
        return self.count(s) / float(self.N)

    '''
    confidence(S) = count(S) / count(L)

    s - the set of Left U Right
    l - the set of the Left side of the rule
    '''
    def confidence(self, s, l):
        return self.count(s) / float(self.count(l))

    '''
    lift = support (s) / (support(l) * support(r))
    '''
    def lift(self, s, r, l):
        return self.support(s)/(self.support(l)*self.support(r))

    '''
    This will take "s" ("--   ((idx1, val1), (idx2, val2), ...)   --"), a
    tuple of tuples and determine the # of transactions that tuple of tuples
    appears in.
    '''
    def count(self, s):
        i = 0

        s_tup = tuple(s)

        # if count is cached return it
        if s_tup in self.count_cache:
            return self.count_cache[s_tup]

        s_set = set(s)
        for transaction in self.sets:
            if s_set < transaction:
                i += 1

        self.count_cache[s_tup] = i

        return i

    '''
    returns the excel formatted string of the table (CSV format)
    '''
    def __str__(self):
        s = ""
        for row in self.itemset:
            for i in range(0, len(row)):
                col = row[i][1]
                if i != 0:
                    s += ','
                s += str(col)
            s += "\n"
        return s

    '''
    Returns L:
        All possible itemsets that can be used to make rules
    '''
    def apriori(self, support_min):
        L = list()   # used to store all L's concatinated
        tempL_1 = list(self.domain)
        L_1 = list()

        # make L_1 a list of lists
        for itemset in tempL_1:
            temp = list()
            temp.append(itemset)
            L_1.append(temp)

        # call the helper recursive function
        return self.__apriori__(L_1, L, support_min)

    def __apriori__(self, L_k, glob_L, support_min):
        # get the candidate set
        C_k = self.get_candidate(L_k, len(L_k))

        # filter out the itemsets that don't have enough support
        newL = self.support_filter(C_k, support_min)

        # append the new L set to the glob L set
        if len(newL) == 0:
            return L_k

        return self.__apriori__(newL, glob_L, support_min)


    def rule_gen(self, confidence_min, support_min):
        itemsets = self.apriori(support_min)


        rules = []
        bad_rhs = set()

        for itemset in itemsets:
            for i in xrange(1, len(itemset) - 1):
                keep_looking = False
                for rhs in combs(itemset, i):
                    if len(rhs) == len(itemset) or len(rhs) == 0:
                        continue

                    if tuple(rhs) in bad_rhs:
                        continue

                    lhs = self.get_lhs(rhs, itemset)

                    if self.confidence(itemset, lhs) > confidence_min:
                        rules.append((lhs, rhs))
                        keep_looking = True
                    else:
                        bad_rhs.add(tuple(rhs))
                if not keep_looking:
                    break
        return rules

    def print_rules(self, confidence_min, support_min):
        rules = self.rule_gen(confidence_min, support_min)


        header = ["Rule", "Support", "Confidence", "Lift"]

        association_rules = []
        for rule in rules:
            row = "IF "
            for i in range(0, len(rule[0])):
                 row += self.attrs[rule[0][i][0]] + " = " + str(rule[0][i][1])
                 if i == (len(rule[0]) - 1):
                     row += " THEN "
                     break
                 row += " and "

            for i in range(0, len(rule[1])):
                 row += self.attrs[rule[1][i][0]] + " = " + str(rule[1][i][1])
                 if i != (len(rule[1]) - 1):
                     row += " and "


            rhs = []
            for r in rule[1]:
                rhs.append(r)

            lhs = []
            for l in rule[0]:
                lhs.append(l)

            s = rhs + lhs
            #print s
            #print lhs

            association_rules.append([row, self.support(s), self.confidence(s, lhs), self.lift(s, lhs, rhs)])

        print "\n"
        print tabulate(association_rules, header, tablefmt="rst")
        print "\n"


    '''
    This will return the candidate set C_k
    '''
    def get_candidate(self, L, lLength):
        C_k = []
        for i in xrange(len(L)):
            for j in xrange(i+1, len(L)):
                a = L[i]
                b = L[j]

                if a[:-1] == b[:-1]:
                    # we found a match
                    union = a + [b[-1]]

                    k_minus_1_ss = self.get_k_minus_one_subsets(union)

                    is_union_supported = True
                    for subset in k_minus_1_ss:
                        if subset not in L:
                            is_union_supported = False
                            break

                    if is_union_supported:
                        C_k.append(union)

        return C_k

    '''
    This function will return newL, the Candidate set that was passed in
    w/o all itemsets that don't have at least the minimum support
    '''
    def support_filter(self, C_k, support_min):
        newL = []
        for itemset in C_k:
            if self.support(itemset) >= support_min:
                newL.append(itemset)
        return newL


    '''
    Returns a list of all possible combinations of size k-1 of the itemset
    '''
    def get_k_minus_one_subsets(self, items):
        K = len(items)
        return [items[:i] + items[i+1:] for i in range(K)]


    '''
        Given S, and a rhs return the lhs
    '''
    def get_lhs(self, rhs, s):
        return filter(lambda x: x not in rhs, s)
