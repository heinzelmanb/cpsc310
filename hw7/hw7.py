from Table import Table
from Itemset import Itemset

if __name__ == "__main__":
        mushroom_table = Table(file="agaricus-lepiota.txt")

        titanic_table = Table(file="titanic.txt")

        mushroom_attrs = ["Class Label", "Cap Shape","Cap Surface",
                "Cap Color", "Bruises","Odor","Gill Attachment",
                "Gill Spacing", "Gill Size","Gill Color", "Stalk Shape",
                "Stalk Root","Stalk Surface Above Ring",
                "Stalk Surface Below Ring", "Stalk Color Above Ring",
                "Stalk Color Below Ring", "Veil Type", "Veil Color",
                "Ring Number", "Ring Type","Spore Pring Color",
                "Population", "Habitat"]

        titanic_attrs = ["class", "age", "sex", "survived"]

        mushroom_itemset = Itemset(table=mushroom_table, attrs=mushroom_attrs)
        titanic_itemset =  Itemset(table=titanic_table, attrs=titanic_attrs)

        rules = mushroom_itemset.print_rules(.9, .95)

        rules = titanic_itemset.print_rules(.8, .2)
