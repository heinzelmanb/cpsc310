'''
    Authors: Bert Heinzelman, Bradley Carrion
    Date: September 5, 2016
    Assignment: hw1
    File: hw1.py
'''

import csv
from tabulate import tabulate

'''
    class to handle table operations
'''


class Table(object):

    def __init__(self, **kwargs):
        file = kwargs.get('file', None)
        inTable = kwargs.get('table', None)

        if file is not None:
            self.table = self.get_table(file)
        elif inTable is not None:
            self.table = inTable

        self.file = file
    '''
        returns a csv table, list of list
    '''

    def get_table(self, file):
        with open(file, 'r') as f:
            reader = csv.reader(f, dialect="excel")

            return [row for row in reader if len(row) > 0]

    '''
        returns the number of instances in a table, int
    '''

    def count_rows(self):
        return len(self.table)

    '''
        returns a list containing the indices
        of the duplicate rows
    '''

    def get_dups(self, keys):
        st = set()
        dups = []

        for j in range(0, len(self.table)):
            row = self.table[j]
            tup_row = tuple([row[i] for i in range(0, len(row)) if i in keys])
            if tup_row in st:
                dups.append(j)
            else:
                st.add(tup_row)

        return dups

    def join(self, keysMe, tableB, keysB):
        ''' gets the key values as a tuple, must be a tuple so it can be hashed'''

        if len(tableB.table) == 0:
            return self.tableB
        elif len(self.table) == 0:
            return tableB

        tableBColNum = len(tableB.table[0])
        myColNum = len(self.table[0])

        # returns the keys as a tuple
        keys_tuple = lambda keys, row: tuple([row[key] for key in keys])

        # hash the values in table b for O(1) look up
        bDict = {}

        for row in tableB.table:
            key_tup = keys_tuple(keysB, row)

            # adding tuple with row, and boolean to see if the row
            # has been used yet. useful for right outer joins
            if key_tup not in bDict:
                bDict[key_tup] = [(row, False)]
            else:
                bDict[key_tup].append((row, False))

        joinedTable = []

        for row in self.table:
            matchKey = keys_tuple(keysMe, row)

            matchedRows = bDict.get(matchKey, None)

            if matchedRows is None:
                # for left outer join, add row with NA's
                joinedTable.append(
                    row + ['NA' for i in range(0, tableBColNum - len(keysB))])
            else:
                for r in matchedRows:
                    new_row = []
                    for i in range(0, len(r[0])):
                        if i not in keysB:
                            new_row.append(r[0][i])

                    joinedTable.append(row + new_row)

                # mark the rows in matched rows used
                bDict[matchKey] = [(row[0], True) for row in matchedRows]

        # for right outer joinedTable
        for key in bDict:
            item = bDict[key]
            # loop through matches for key
            for row in item:
                if row[1] == False:
                    emptyRow = ['NA' for i in range(0, myColNum)]
                    # put keys from row b into row a
                    for j in range(0, len(keysMe)):
                        mKey = keysMe[j]
                        bKey = keysB[j]

                        emptyRow[mKey] = row[0][bKey]

                    # remove keys
                    row = ([row[0][i] for i in range(0, len(row[0]))
                            if not(i in keysB)], row[1])
                    joinedTable.append(emptyRow + row[0])

        return Table(table=joinedTable)

    def get_column(self, column):
        return [float(row[column])
                for row in self.table if row[column] != 'NA']

    # return the min of the table
    def min(self, column):
        return min(self.get_column(column))

    # returns the max of the table
    def max(self, column):
        return max(self.get_column(column))

    # returns the midpoint of the table
    def midpoint(self, column):
        return (self.max(column) + self.min(column)) / 2.0

    # returns the midpoint of the table
    def average(self, column):
        col = self.get_column(column)
        entries = len(col)
        return sum(col) / float(entries)

    # returns the median of the table
    def median(self, column):
        col = sorted(self.get_column(column))
        mid = len(col) / 2

        if len(col) % 2 == 0:
            midA = col[mid - 1]
            midB = col[mid]

            return (midA + midB) / 2
        else:
            return col[mid]

    # returns the excel formatted string of the table
    def __str__(self):
        s = ""
        for row in self.table:
            for i in range(0, len(row)):
                col = row[i]
                if i != 0:
                    s += ','
                s += str(col)
            s += "\n"
        return s

    # export method will export the table in a file
    # required! Must specify the file name variable in the object
    #           before calling this function
    def export(self):
        with file(self.file, "w") as f:
            f.write(str(self))


'''
    Displays some basic information about the table
'''


def display_table_info(table, keys):
    divider = '-' * 50

    print divider
    print table.file,
    print ':'
    print divider

    print "No. of instances: ",
    print table.count_rows()
    print "Duplicates: ",
    print [table.table[dup] for dup in table.get_dups(keys)]
    print ''

'''
    Displays extended statistics on continuous columns
    table: the table to display stats on
    keys : the keys which you want stats on as a list of tuples
            [(index, "name of attribute"), ... ]
'''

def table_stats(table, keys):
    stats = []
    headers = ['attribute', 'min', 'max', 'mid', 'avg', 'med']

    for key in keys:
        idx = key[0]
        name = key[1]
        row = [name]

        # add the stats to the row
        row.append(format(table.min(idx), '.2f'))
        row.append(format(table.max(idx), '.2f'))
        row.append(format(table.midpoint(idx), '.2f'))
        row.append(format(table.average(idx), '.2f'))
        row.append(format(table.median(idx), '.2f'))

        stats.append(row)
    print "Summary Statistics: "
    print tabulate(stats, headers, tablefmt="rst")

# this function returns a new table with instnaces with NA removed
def remove_instances_missing_vals(table, continuous_attr, keys):
    temp_table = []

    # getting only instances w/o 'NA'
    for row in table.table:
        if not "NA" in row:
            temp_table.append(row)

    export_table = Table(table=temp_table)
    export_table.file = "removed-na.txt"
    export_table.export()

    # display basic info and stats
    display_table_info(export_table, keys)
    table_stats(export_table, continuous_attr)

# returns a new table with NA's in continuous attributes replaced with averages
def instert_averages_where_missing_vals(table, continuous_attr, keys):
    temp_table = [] # new table to be exported
    avg_vals = {}   # stores the averages of all continuous attributes

    # retrieving averages for only continuous attributes
    for col in continuous_attr:
        avg_vals[col[0]] = table.average(col[0])

    # loop through each instance
    for instance in table.table:
        copy = instance[:]  # creates a copy of the instance

        # loop through each continuous attribute
        for i in continuous_attr:

            # check for NA values
            if copy[i[0]] == "NA":
                copy[i[0]] = avg_vals[i[0]] # replace vals with average
        temp_table.append(copy)

    export_table = Table(table=temp_table)
    export_table.file = "avg_vals.txt"
    export_table.export()

    # display basic info and stats
    display_table_info(export_table, keys)
    table_stats(export_table, continuous_attr)

# exports a new table with NA's removed and replaced with educated estimates
def smart_missing_vals_handler(table, continuous_attr, keys):
    temp_table = [] # new table to be exported

    # loop through all instances in the table
    for instance in table.table:
        copy = instance[:]  # creates a copy of the instance

        # loop throught the attributes
        for i in continuous_attr:

            # check if the field is NA
            if copy[i[0]] == "NA":

                # get the average of other instances with same name
                avg = carname_average(table, instance[8], i[0])

                # check if there are no other instances
                if avg is None:
                    avg = table.average(i[0])
                copy[i[0]] = avg    # change the value from NA
        temp_table.append(copy)

    export_table = Table(table=temp_table)
    export_table.file = "smart_avg_vals.txt"
    export_table.export()

    # display basic info and stats
    display_table_info(export_table, keys)
    table_stats(export_table, continuous_attr)

# returns the average of other instances with same name
def carname_average(table, car_name, index):
    car_rows = filter(lambda row: row[8] == car_name and row[
                      index] != 'NA', table.table)
    car_rows = [float(row[index]) for row in car_rows]

    if len(car_rows) == 0:
        return None
    return float(sum(car_rows)) / len(car_rows)


def main():
    # primary keys for each table
    mpg_keys = [8, 6]
    prices_keys = [0, 1]

    mpg = Table(file='auto-mpg-nodup.txt')
    prices = Table(file='auto-prices-nodup.txt')

    # create all data table
    joined = mpg.join(mpg_keys, prices, prices_keys)

    # removed rows with no mpg data but with msrp
    joined.table = [
        row for row in joined.table if not (
            row[0] == "NA" and row[9] != "NA")]

    joined.file = "auto-data.txt"

    joined.export()

    # display basic info of tables
    display_table_info(mpg, mpg_keys)
    display_table_info(prices, prices_keys)
    display_table_info(joined, mpg_keys)

    stat_columns = [(0, 'mpg'), (2, 'displacement'), (3, 'horsepower'),
                    (4, 'weight'), (5, 'acceleration'), (9, 'MSRP')]

    # display extended stats for joined table
    table_stats(joined, stat_columns)

    # joined.export()
    # resolve missing values by removing instances
    remove_instances_missing_vals(joined, stat_columns, mpg_keys)
    # resolve missing values by taking the average
    instert_averages_where_missing_vals(joined, stat_columns, mpg_keys)
    # resolve missing values by educated factors
    smart_missing_vals_handler(joined, stat_columns, mpg_keys)

if __name__ == '__main__':
    main()
