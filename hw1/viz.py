
import matplotlib
matplotlib.use('pdf')
import matplotlib.pyplot as pyplot

import numpy

# FIGURE 1

xs = [1,2,3,4,5,6,7]
ys = [1,1,2,3,5,8,13]
pyplot.plot(xs, ys)
pyplot.savefig('fig1.pdf')

# FIGURE 2

pyplot.figure()
xs = ['foo','bar','baz','quz']
ys = [100,200,300,400]
# calculate a range (make y a bit bigger)
xrng = numpy.arange(len(xs))
yrng = numpy.arange(0, max(ys) + 100, 50)
# create the bar chart
pyplot.bar(xrng, ys, 0.45, align='center')
# define the x and y ranges (and value labels)
pyplot.xticks(xrng, xs)
pyplot.yticks(yrng)
pyplot.grid(True)
pyplot.savefig('fig2.pdf')

# FIGURE 3

pyplot.figure()
mu, sigma = 100, 5
xs = numpy.random.normal(mu, sigma, 1000)
pyplot.hist(xs, bins=5, alpha=0.95, color='r')
pyplot.savefig('fig3.pdf')

# FIGURE 4

pyplot.figure(figsize=(8,8))
xs = ['foo','bar','baz','qux']
ys = [100,200,300,400]
pyplot.pie(ys, labels=xs, autopct='%1.1f%%')
pyplot.savefig('fig4.pdf')

# FIGURE 5

pyplot.figure()
mu, sigma = 100, 5
xs = numpy.random.normal(mu, sigma, 1000)
xs.sort()
ys = [1] * len(xs)
pyplot.plot(xs, ys, 'g.', alpha=0.2, markersize=6)
pyplot.gca().get_yaxis().set_visible(False)
pyplot.savefig('fig5.pdf')

# FIGURE 6

pyplot.figure()
ys = numpy.random.choice(2000, 100, replace=False)
xs = list(range(len(ys)))
pyplot.plot(xs, ys, 'b.')
pyplot.xlim(0, int(max(xs) * 1.1))
pyplot.ylim(0, int(max(ys) * 1.1))
pyplot.grid(True)
pyplot.savefig('fig6.pdf')

# FIGURE 7

pyplot.figure()
mu, sigma = 100, 5
xs1 = numpy.random.normal(mu, sigma, 1000)
xs2 = numpy.random.normal(mu, sigma, 100)
pyplot.boxplot([xs1, xs2])
pyplot.xticks([1,2], ['1000 samples', '100 samples'])
pyplot.savefig('fig7.pdf')

# FIGURE 8

pyplot.figure()
xs1 = [10,20,30,40]
xs2 = [30,20,20,30]
fig, ax = pyplot.subplots()
r1 = ax.bar([1,2,3,4], xs1, 0.3, color='r')
r2 = ax.bar([1.3,2.3,3.3,4.3], xs2, 0.3, color='b')
ax.set_xticks([1.3,2.3,3.3,4.3])
ax.set_yticks([10,20,30,40,50])
ax.set_xticklabels(['Val1','Val2','Val3','Val4'])
ax.legend((r1[0],r2[0]), ('Bar1','Bar2'), loc=2)
pyplot.grid(True)
pyplot.savefig('fig8.pdf')
pyplot.close()


