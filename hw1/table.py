'''
    Authors: Bert Heinzelman, Bradley Carrion
    Date: September 21, 2016
    File: Table Class
'''

import csv
from tabulate import tabulate

'''
    class to handle table operations
'''


class Table(object):

    def __init__(self, **kwargs):
        file = kwargs.get('file', None)
        inTable = kwargs.get('table', None)

        if file is not None:
            self.table = self.get_table(file)
        elif inTable is not None:
            self.table = inTable

        self.file = file
    '''
        returns a csv table, list of list
    '''

    def get_table(self, file):
        with open(file, 'r') as f:
            reader = csv.reader(f, dialect="excel")

            return [row for row in reader if len(row) > 0]

    '''
        returns the number of instances in a table, int
    '''

    def count_rows(self):
        return len(self.table)

    '''
        returns a list containing the indices
        of the duplicate rows
    '''

    def get_dups(self, keys):
        st = set()
        dups = []

        for j in range(0, len(self.table)):
            row = self.table[j]
            tup_row = tuple([row[i] for i in range(0, len(row)) if i in keys])
            if tup_row in st:
                dups.append(j)
            else:
                st.add(tup_row)

        return dups

    def join(self, keysMe, tableB, keysB):
        ''' gets the key values as a tuple, must be a tuple so it can be hashed'''

        if len(tableB.table) == 0:
            return self.tableB
        elif len(self.table) == 0:
            return tableB

        tableBColNum = len(tableB.table[0])
        myColNum = len(self.table[0])

        # returns the keys as a tuple
        keys_tuple = lambda keys, row: tuple([row[key] for key in keys])

        # hash the values in table b for O(1) look up
        bDict = {}

        for row in tableB.table:
            key_tup = keys_tuple(keysB, row)

            # adding tuple with row, and boolean to see if the row
            # has been used yet. useful for right outer joins
            if key_tup not in bDict:
                bDict[key_tup] = [(row, False)]
            else:
                bDict[key_tup].append((row, False))

        joinedTable = []

        for row in self.table:
            matchKey = keys_tuple(keysMe, row)

            matchedRows = bDict.get(matchKey, None)

            if matchedRows is None:
                # for left outer join, add row with NA's
                joinedTable.append(
                    row + ['NA' for i in range(0, tableBColNum - len(keysB))])
            else:
                for r in matchedRows:
                    new_row = []
                    for i in range(0, len(r[0])):
                        if i not in keysB:
                            new_row.append(r[0][i])

                    joinedTable.append(row + new_row)

                # mark the rows in matched rows used
                bDict[matchKey] = [(row[0], True) for row in matchedRows]

        # for right outer joinedTable
        for key in bDict:
            item = bDict[key]
            # loop through matches for key
            for row in item:
                if row[1] == False:
                    emptyRow = ['NA' for i in range(0, myColNum)]
                    # put keys from row b into row a
                    for j in range(0, len(keysMe)):
                        mKey = keysMe[j]
                        bKey = keysB[j]

                        emptyRow[mKey] = row[0][bKey]

                    # remove keys
                    row = ([row[0][i] for i in range(0, len(row[0]))
                            if not(i in keysB)], row[1])
                    joinedTable.append(emptyRow + row[0])

        return Table(table=joinedTable)

    def get_column(self, column):
        return [float(row[column])
                for row in self.table if row[column] != 'NA']

    # return the min of the table
    def min(self, column):
        return min(self.get_column(column))

    # returns the max of the table
    def max(self, column):
        return max(self.get_column(column))

    # returns the midpoint of the table
    def midpoint(self, column):
        return (self.max(column) + self.min(column)) / 2.0

    # returns the midpoint of the table
    def average(self, column):
        col = self.get_column(column)
        entries = len(col)
        return sum(col) / float(entries)

    # returns the median of the table
    def median(self, column):
        col = sorted(self.get_column(column))
        mid = len(col) / 2

        if len(col) % 2 == 0:
            midA = col[mid - 1]
            midB = col[mid]

            return (midA + midB) / 2
        else:
            return col[mid]

    # returns the excel formatted string of the table
    def __str__(self):
        s = ""
        for row in self.table:
            for i in range(0, len(row)):
                col = row[i]
                if i != 0:
                    s += ','
                s += str(col)
            s += "\n"
        return s

    # export method will export the table in a file
    # required! Must specify the file name variable in the object
    #           before calling this function
    def export(self):
        with file(self.file, "w") as f:
            f.write(str(self))
