from Classifiers import DecisionTreeClassifier
from Table import Table

def print_dotfile(tree):
    with open("graph.dot", "w") as f:
        f.write("graph g {\nrank = TB;\n")
        __print_dotfile__(tree, f, None, 0, 0, 0, "")
        f.write("}")

def __print_dotfile__(node, f, parent, c_id, p_id, node_type, label):
        if node_type == 1:  # value node
            if node.class_label is not None: #we hit a leaf
                # icrement c
                c_id += 1

                # create the final node
                f.write("N" + str(c_id) + " [label=\"result: " + str(node.class_label) + "\" shape=circle];\n")
                # connenct the final node to the p_id
                f.write("N" + str(c_id) + " -- N" + str(p_id) + ";\n")
            else: # average value node
                c_id += 1
                f.write("N" + str(c_id) + " [label=\"value: " + str(key) + "\" shape=circle];\n")
                f.write("N" + str(c_id) + " -- N" + str(p_id) + ";\n")
                p_id = c_id
                __print_dotfile__(node, f, parent, c_id, p_id, 0, "")
        else:   # attribute node
            # increment c
            c_id += 1

            # create the attribute node
            f.write("N" + str(c_id) + " [label=\"attribute: " + str(node.attribute_idx) + "\" shape=box];\n")
            # connect the final node to the p_id
            f.write("N" + str(c_id) + " -- N" + str(p_id) + ";\n")

            #set the p_id to the c_id
            p_id = c_id

            for key in node.children.keys():
                child = node.children[key]
                __print_dotfile__(child, f, node, c_id, p_id, 1, key)

if __name__ == "__main__":
        titanic_data = Table(file="titanic.txt")
        titanic_data.table = titanic_data.table[1:]

        tree_classifier = DecisionTreeClassifier(3, [0, 1, 2], titanic_data)

        print_dotfile(tree_classifier.tree)
