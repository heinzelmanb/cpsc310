'''
    Authors: Bert Heinzelman, Bradley Carrion
    Date: 10/25/16
    Assignment: 5
'''

import numpy
from Table import Table
from Classifiers import DecisionTreeClassifier, BayesianClassifier, KNNClassifier
from Test import CrossValidation
import copy

'''
    Given a weight, discritize it
'''
def weight_discritize(val):
    val = float(val)
    if val >= 3500:
        return 5
    elif val >= 3000:
        return 4
    elif val >= 2500:
        return 3
    elif val >= 2000:
        return 2
    return 1

'''
    Given an mpg, discritize it
'''
def mpg_discritize(value):
    value = float(value)
    if value >= 45:
        return 10
    elif value >= 37:
        return 9
    elif value >= 31:
        return 8
    elif value >= 27:
        return 7
    elif value >= 24:
        return 6
    elif value >= 20:
        return 5
    elif value >= 17:
        return 4
    elif value >= 15:
        return 3
    elif value >= 14:
        return 2
    else:
        return 1

def discritize(table, function, idx):
    table_copy = copy.deepcopy(table)

    for row in table_copy.table:
        row[idx] = function(row[idx])
    return table_copy

if __name__ == "__main__":
    titanic_data = Table(file="titanic.txt")
    titanic_data.table = titanic_data.table[1:]

    print "==============================="
    print "STEP 1"
    print "==============================="
    baye_classifier = BayesianClassifier(3, [0, 1, 2], titanic_data)
    knn_classifer = KNNClassifier(3, [0, 1, 2], [0, 1, 2], 10, titanic_data)
    tree_classifier = DecisionTreeClassifier(3, [0, 1, 2], titanic_data)

    print
    print "==============================="
    print "STEP 1 RULES"
    print "==============================="
    rules = tree_classifier.get_rules()
    for rule in rules:
        print rule

    step1a = CrossValidation(tree_classifier, 10, titanic_data)
    step1a.run_test()

    step3c = CrossValidation(baye_classifier, 10, titanic_data)
    step3c.run_test()

    step3c = CrossValidation(knn_classifer, 10, titanic_data)
    step3c.run_test()

    print "==============================="
    print "STEP 2"
    print "==============================="
    auto_data = discritize(Table(file="auto-data.txt"), weight_discritize, 4)
    auto_data = discritize(auto_data, mpg_discritize, 0)

    tree_classifier = DecisionTreeClassifier(0, [1, 4, 6], auto_data)

    print
    print "==============================="
    print "STEP 2 RULES"
    print "==============================="
    rules = tree_classifier.get_rules()
    for rule in rules:
        print rule

    step2 = CrossValidation(tree_classifier, 10, auto_data)
    step2.run_test()
