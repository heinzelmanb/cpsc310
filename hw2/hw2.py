'''
    Authors: Bert Heinzelman, Bradley Carrion Date: September 21, 2016
    Assignment: hw2
    File: hw2.py
'''

import numpy
import matplotlib
matplotlib.use('pdf')
import matplotlib.pyplot as pyplot
import csv

'''
    class to handle table operations
'''
class Table(object):

    def __init__(self, **kwargs):
        file = kwargs.get('file', None)
        inTable = kwargs.get('table', None)

        if file is not None:
            self.table = self.get_table(file)
        elif inTable is not None:
            self.table = inTable

        self.file = file

    def get_table(self, file):
        with open(file, 'r') as f:
            reader = csv.reader(f, dialect="excel")

            return [row for row in reader if len(row) > 0]

    '''
        returns the number of instances in a table, int
    '''

    def count_rows(self):
        return len(self.table)


    '''
        returns a lists of lists grouped together based on
        another attribute
    '''
    def group_by(self, index):
        groups = {}
        for row in self.table:
            if row[index] in groups:
                groups[row[index]].append(row)
            else:
                groups[row[index]] = [row]
        # fix maybe
        group_list = sorted([groups[key] for key in groups.keys()], key=lambda row: row[0][index])
        #group_list = [groups[key] for key in groups.keys()]
        return [Table(table=group) for group in group_list]


    def get_column(self, column):
        return [float(row[column])
                for row in self.table if row[column] != 'NA']

    # return the min of the table
    def min(self, column):
        return min(self.get_column(column))

    # returns the max of the table
    def max(self, column):
        return max(self.get_column(column))

    # returns the midpoint of the table
    def average(self, column):
        col = self.get_column(column)
        entries = len(col)
        return sum(col) / float(entries)


'''
This method calculates the values needed for the
best fit line to place on this scatter plot. This will
return a tuple where the first value will be the
slope of the 'best fit' line and the second value
will be the y-intercept. With these values we can
graph the 'best fit line'
'''
def least_squares(table, index_x, index_y):
    xs = table.get_column(index_x)
    ys = table.get_column(index_y)
    x_avg = table.average(index_x)
    y_avg = table.average(index_y)

    numerator = float(sum([(xs[i] - x_avg) * (ys[i] - y_avg)for i in range(len(xs))]))
    denomenator = float(sum([(xi - x_avg)**2 for xi in xs]))

    # calculate the slope
    m = numerator / denomenator

    # calculate the y-intercept
    b = y_avg - m*x_avg

    return (m, b)

'''
This method calculates the correlation coefficient between
the values of the data points of records for attributes
at indexes x and y.
'''
def correlation_coefficient(table, index_x, index_y):
    xs = table.get_column(index_x)
    ys = table.get_column(index_y)
    x_avg = table.average(index_x)
    y_avg = table.average(index_y)

    numerator = float(sum([(xs[i] - x_avg) * (ys[i] - y_avg)for i in range(len(xs))]))
    denomenator = float(sum([(xi - x_avg)**2 for xi in xs]))
    denomenator *= float(sum([(yi - y_avg)**2 for yi in ys]))

    denomenator = denomenator ** (.5)

    return numerator / denomenator


'''
This method generates a bar graph with all required Data
being passed into it with an optional parameter of steps
or distance between the bars
'''
def gen_bar(x_vals, y_vals, filename, x_label, y_label, steps=50):
    pyplot.clf()
    # calculate a range (make y a bit bigger)
    xrng = numpy.arange(len(x_vals))
    yrng = numpy.arange(0, max(y_vals) + max(y_vals) * 0.5, steps)
    # create the bar chart
    pyplot.bar(xrng, y_vals, 0.45, align='center')
    # define the x and y ranges (and value labels)
    pyplot.xticks(xrng, x_vals)
    pyplot.yticks(yrng)
    pyplot.grid(True)
    pyplot.xlabel(x_label)
    pyplot.ylabel(y_label)
    pyplot.title("Frequency of " + x_label)
    pyplot.savefig(filename)

'''
This method generates a pie chart based on the parameters passed in
'''
def gen_pie_chart(x_vals, y_vals, filename, x_label, y_label):
    pyplot.figure(figsize=(8,8))
    pyplot.title("Frequency of " + x_label)
    pyplot.pie(y_vals, labels=x_vals, autopct='%1.1f%%')
    pyplot.savefig(filename)

'''
This method generates a dot diagram based on the parameters passed in
'''
def gen_strip_chart(x_vals, y_vals, filename, xlabel="", title=""):
    pyplot.clf()
    pyplot.plot(x_vals, y_vals, 'g.', alpha=0.2, markersize=6)
    pyplot.gca().get_yaxis().set_visible(False)
    pyplot.xlabel(xlabel)
    pyplot.title(title)
    pyplot.savefig(filename)

'''
This method generates a histogram based on the parameters passed in
and has equal sized bins of size 10
'''
def gen_hist(xs, xlabel, title, filename):
    pyplot.clf()
    pyplot.hist(xs, bins=10, alpha=0.95, color="b")
    pyplot.title(title)
    pyplot.xlabel(xlabel)
    pyplot.ylabel("Count")
    pyplot.savefig(filename)

'''
This method generates a scatter plot based on the values passed in.
If the add_reg parameter is used along with the add_figtext parameter
then a best fit line will be calculated and added to the scatter plot.
'''
def gen_scatter(table, x_index, y_index, xlabel, ylabel, title, filename, add_reg=False, add_figtext=False):
    pyplot.clf()
    pyplot.plot(table.get_column(x_index), table.get_column(y_index), 'b.')
    pyplot.grid(True)
    pyplot.xlabel(xlabel)
    pyplot.ylabel(ylabel)
    pyplot.title(title)
    if add_reg:
        ls = least_squares(table, x_index, y_index)
        min_val = table.min(x_index)
        max_val = table.max(x_index)
        pyplot.plot([0, max_val], [ls[1], ls[0]*max_val+ls[1]], 'k-', lw=2)

    if add_figtext:
        pyplot.annotate("Slope: " + str(numpy.round(ls[0], decimals=3)), xy=(0.4, 0.95),
                xycoords="axes fraction", fontsize=16)

        coef = correlation_coefficient(table, x_index, y_index)

        pyplot.annotate("Correlation Coefficient: " + str(numpy.round(coef, decimals=3)), xy=(0.4, 0.91),
                xycoords="axes fraction",fontsize=16)

    pyplot.savefig(filename)

'''
This method generates a boxplot with a group_by index allowing
us to group by a specific attribute.
'''
def gen_boxplot(table, index, gbi, filename, xlabel, ylabel, title):
    pyplot.clf()
    plots = [tab.get_column(index) for tab in table.group_by(gbi)]

    pyplot.boxplot(plots)

    years = sorted(list(set(table.get_column(gbi))))
    years.sort()

    pyplot.xticks(numpy.arange(1, len(years) + 1, 1), years)
    pyplot.ylabel(ylabel)
    pyplot.xlabel(xlabel)
    pyplot.title(title)
    pyplot.savefig(filename)

'''
This method generates a frequency diagram based on the parameters passed in
which allow us to group by two seperate indexes.
'''
def gen_frequency_diagram(table, index, gbi1, gbi2, filename, xlabel, ylabel, title):
    pyplot.clf()
    gb_country = table.group_by(gbi1)
    country_plot, year_plot = pyplot.subplots()

    different_years = set(table.get_column(gbi2))
    different_origins = set(table.get_column(gbi1))

    country_years = {origin: [0 for year in different_years] for origin in different_origins}

    for country in gb_country:
        cur_origin = country.table[0][gbi1]
        years = country.group_by(gbi2)
        for year in years:
            cur_year = year.table[0][gbi2]
            country_years[int(cur_origin)][int(cur_year) - int(min(different_years))] = year.count_rows()

    # turn dictionary to list
    country_years = [(key, country_years[key]) for key in country_years.keys()]

    #plot the bars
    i = 0
    spacing = numpy.arange(1, (len(different_years)/2.0) + .51, 0.5)
    bars = []
    colors = ['r', 'g', 'b']
    for year in country_years:

        bars.append(year_plot.bar(spacing, year[1], 0.1, color=colors[i%3]))
        spacing = [space + 0.1 for space in spacing]
        i += 1

    # set the labels
    year_plot.set_xticklabels([int(year) for year in different_years])
    pyplot.ylabel(ylabel)
    pyplot.xlabel(xlabel)
    pyplot.title(title)

    #build the key
    year_plot.legend(tuple([r[0] for r in bars]), [i for i in different_origins], loc=2)

    pyplot.grid(True)
    pyplot.savefig(filename)
    pyplot.close()


'''
    returns the frequency list
    for a specific column
'''
def frequency(tbl, index):
    d = {}
    col = tbl.get_column(index)
    for item in col:
        if item in d:
            d[item] += 1
        else:
            d[item] = 1
    sorted_keys = sorted(d.keys())
    return [(key, d[key]) for key in sorted_keys]

'''
This method plots a categorical type of graph based on
the function 'plot' passed in.
'''
def categories(table, plot, pre_file_name):
    categoricals = [(1, 'Cylinders'), (6, 'Model_year'), (7,'Origin')]
    for item in categoricals:
        freq = frequency(table, item[0])
        xs = [category[0] for category in freq]
        ys = [category[1] for category in freq]
        plot(xs, ys, pre_file_name+'-'+item[1]+'.pdf', item[1], 'Count')

'''
This method uses the gen_strip_chart method
'''
def continuous(table, pre_file_name):
    categoricals = [(0, 'Mpg'), (2, 'Displacement'), (3,'Horsepower'), (4,'Weight'), (5,'Acceleration'), (9,'Msrp')]
    for item in categoricals:
        xs = table.get_column(item[0])
        xs.sort()
        ys = [1] * len(xs)
        gen_strip_chart(xs, ys, pre_file_name+'-'+item[1]+'.pdf', item[1], item[1] + " Data")

'''
This method converts continuous data to categorical data
then returns the new list of categorical values. And it does
this conversion by using the map function parameter passed in.
'''
def continuous_to_categorical(table, index, map_func):
    d = {}
    columns = table.get_column(index)
    for val in columns:
        category = map_func(val)
        if category in d:
            d[category] += 1
        else:
            d[category] = 1
    sorted_keys = sorted(d.keys())
    return [(key, d[key]) for key in sorted_keys]

'''
This method carries out the requirements of step 4
'''
def step4(table):

    '''
    This method maps the continuous values to
    specific categorical values based on the value.
    '''
    def map1(value):
        if value >= 45:
            return 10
        elif value >= 37:
            return 9
        elif value >= 31:
            return 8
        elif value >= 27:
            return 7
        elif value >= 24:
            return 6
        elif value >= 20:
            return 5
        elif value >= 17:
            return 4
        elif value >= 15:
            return 3
        elif value >= 14:
            return 2
        else:
            return 1

    freq = continuous_to_categorical(table, 0, map1)
    xs = [val[0] for val in freq]
    ys = [val[1] for val in freq]

    gen_bar(xs, ys, "step4-approach1.pdf", "Mpg", "Count", 10)

    # create ranges for equal bins
    min_val = table.min(0)
    max_val = table.max(0)

    step = (max_val - min_val) / 5.0

    cutoff_vals = numpy.arange(min_val, max_val + step, step)
    cutoff_vals[-1] += 1

    '''
    This method maps the continuous values to categorical values
    based on equal sized seperated bins.
    '''
    def map2(value):
        for i in range(1, len(cutoff_vals)):
            if value >= cutoff_vals[i-1] and value < cutoff_vals[i]:
                return i

    freq = continuous_to_categorical(table, 0, map2)
    xs = [val[0] for val in freq]
    ys = [val[1] for val in freq]

    gen_bar(xs, ys, "step4-approach2.pdf", "Mpg", "Count", 10)

'''
This method carries out the requirements of step 5
'''
def step5(table):
    continuous = [(0, 'Mpg'), (2, 'Displacement'), (3,'Horsepower'), (4,'Weight'), (5,'Acceleration'), (9,'Msrp')]

    for item in continuous:
        xs = table.get_column(item[0])
        xs.sort()
        gen_hist(xs, item[1], item[1] + " Distribution", "step5-" + item[1] + ".pdf")

'''
This method carries out the requirements of steps 6 and 7
'''
def step6and7(table):
    continuous = [(2, 'Displacement'), (3,'Horsepower'), (4,'Weight'), (5,'Acceleration'), (9,'Msrp')]
    for item in continuous:
        # scatter
        gen_scatter(table, item[0], 0, item[1], "MPG", "MPG vs " + item[1], "step6-"+item[1]+".pdf")

        # scatter with regression
        gen_scatter(table, item[0], 0, item[1], "MPG", "MPG vs " + item[1], "step7-"+item[1]+".pdf", True, True)

'''
This method carries out the requirements of step 8
'''
def step8(table):
    # part 1
    gen_boxplot(table, 0, 6, "step8-mpgboxplotbyyear", "Years", "MPG", "MPG by Year")

    # part 2
    title = "Total number of Cars by Year and Origin"
    gen_frequency_diagram(table, 0, 7, 6, "step8-part2.pdf", "Model Year", "Count", title)


if __name__ == '__main__':
    table = Table(file='auto-data.txt')
    categories(table, gen_bar, 'step1')
    categories(table, gen_pie_chart, 'step2')
    continuous(table, 'step3')
    step4(table)
    step5(table)
    step6and7(table)
    step8(table)
